const express = require("express");
const nunjucks = require("nunjucks");

const app = express();

nunjucks.configure("views", {
  autoescape: true,
  express: app,
  watch: true
});

app.use(express.urlencoded({ extended: false }));
app.set("view engine", "njk");

const middlewareCheckAge = (req, res, next) => {
  const age = req.params.age;
  if (parseInt(age) && age > 0) return next();
  return res.redirect("/");
};

app.get("/", (req, res) => {
  return res.render("formAge", { idade: 10 });
});

app.post("/check", (req, res) => {
  const age = req.body.age;
  if (parseInt(age) >= 18) return res.redirect(`/major/${age}`);
  return res.redirect(`/minor/${age}`);
});

app.get("/major/:age", middlewareCheckAge, (req, res) => {
  return res.render("major", { age: req.params.age });
});

app.get("/minor/:age", middlewareCheckAge, (req, res) => {
  return res.render("minor", { age: req.params.age });
});

app.listen(3000);
